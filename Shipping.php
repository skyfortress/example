<?php
/**
 * Created by PhpStorm.
 * User: АРТ
 * Date: 02.02.15
 * Time: 12:13
 */
require_once(DIR_SYSTEM.'library/NovaPoshtaApi2.php');
use LisDev\Delivery\NovaPoshtaApi2;

class Shipping {
    private $apiKey = "1c8d2c6eda12bf420b9e7f24f8ccd64f";
    private $cache;
    private $city_id = 4;
    private $departaments;
    private $streets = [];

    public function __construct($registry)
    {
        $this->cache = $registry->get('cache');

        if (!$this->departaments=$this->cache->get('np_depataments')){

            $jsonData = json_decode(file_get_contents(DIR_SYSTEM.'warehouse.json'));

            $departaments  = array();

            foreach($jsonData->response as $departament){
                if (!isset($departaments[$departament->city_id]))
                    $departaments[$departament->city_id] = array();
                array_push($departaments[$departament->city_id] , $departament);
            }

            $this->cache->set('np_depataments', $departaments);
            $this->departaments = &$departaments;
        }

    }
    public function setCity($city_id)
    {
        if($city_id != 0)
            $this->city_id = (int)$city_id;
    }
    public function renderDepartaments($departament = 1)
    {
        $html = '';
        foreach ($this->departaments[$this->city_id] as $item){
            $selected = ($item->number == $departament) ? 'selected' : '';
            $html .= "<option value='".$item->number."' ". $selected .">".$item->addressRu."</option>";
        }

        return $html;
    }
    public function getDepartament($departament_id)
    {
        foreach ($this->departaments[$this->city_id] as $item){
            if($departament_id == $item->number)
                return $item->addressRu;
        }

    }
    public function getStreets($search)
    {
        $this->getAllStreets();
        return json_encode($this->searchStreets($search));
    }

    private function getAllStreets()
    {
        if ( ! $this->streets=$this->cache->get('kiev_streets')) {

            $data = $this->makeApiRequest();

            $this->cache->set('kiev_streets', $data);
            $this->streets = &$data;
        }
    }

    private function &makeApiRequest()
    {
        $np = new NovaPoshtaApi2($this->apiKey);
        $np->setFormat('array');
        $np->setLanguage('ru');

        $data = [];
        $i = 0;
        do {
            $result = $np->getStreet('8d5a980d-391c-11dd-90d9-001a92567626','', ++$i);
            $data = array_merge($data, $result['data']);

        } while (count($result['data']) == 500);
        return $data;
    }

    private function &searchStreets($search){
        $results = [];
        foreach ($this->streets as $street){
            if (strpos(mb_strtolower($street['Description']), mb_strtolower($search)) !== false)
                $results[] = [
                    'type' => $street['StreetsType'],
                    'Description' => $street['Description']
                ];
        }

        return $results;
    }

    public function getKievRegions(){
        return [
            '---Выберите---',
            'Голосеевский',
            'Днепровский',
            'Подольский',
            'Шевченковский',
            'Дарницкий',
            'Оболонский',
            'Святошинский',
            'Деснянский',
            'Печерский',
            'Соломенский'
        ];
    }

} 