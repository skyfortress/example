<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\Project;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Post;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\web\HttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\db\Connection;
use DisqusAPI;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $pageName = '';

    public function init()
    {
        Yii::$app->layout = 'page';
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        Yii::$app->layout = 'main';

        $db = Yii::$app->getDb();

        $data = $db->cache(function () {
            return Post::find()
                ->joinWith('category')
                ->addOrderBy(['updated' => SORT_DESC])
                ->limit(4)
                ->all();
        });


        return $this->render('index', array('posts' => $data));
    }

    public function actionPortfolio()
    {
        $data = Project::find()->all();

        if (empty($data))
            throw new HttpException(200, 'Проектов не найдено');

        return $this->render('portfolio', ['data' => $data]);
    }

    public function actionBlog()
    {
        unset(Yii::$app->session['lastPost']);

        $category = Yii::$app->request->get('category');
        $page = (int)Yii::$app->request->get('page', 1);


        $data = Post::find()
            ->joinWith('category')
            ->offset(($page * 5) - 5)
            ->addOrderBy(['updated' => SORT_DESC]);

        if ($category) {
            $data->andWhere(['category.url' => $category]);
        }
        $posts = $data->limit(5)->all();


        $count = Yii::$app->getDb()->cache(function () {
            return Post::find()->count();
        });

        $pages = new Pagination([
            'totalCount' => $count,
            'defaultPageSize' => 5,
        ]);

        $this->render('blog', array(
            'data' => $posts,
            'pages' => $pages
        ));
    }

    public function actionPost()
    {
        $postUrl = Yii::$app->request->get('post');
        if ($postUrl) {
            $data = Post::find()
                ->joinWith('category')
                ->where(array("post.url" => $postUrl))
                ->one();

            if (!$data) {
                throw new HttpException(404, 'Пост не найден');
            } else {
                $postId = $data->attributes['post_id'];

                $key = 'counter' . $postId;
                $cacheData = Yii::$app->cache->get($key);

                if (!isset(Yii::$app->session['lastPost']) || Yii::$app->session['lastPost'] != $postId) {

                    //Update views in cache
                    if ($cacheData === false) {
                        $cacheData = 1;
                        Yii::$app->cache->set($key, $cacheData);
                    } else {
                        $cacheData++;
                        Yii::$app->cache->set($key, $cacheData);
                    }
                }
                Yii::$app->session['lastPost'] = $postId;

            }
        } else {
            throw new HttpException(404, 'Категория не найдена');
        }
        return $this->render('post', array('post' => $data));
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        $this->view->registerCssFile('/css/spinner.css');

        return $this->render('about');
    }


    public function actionProject()
    {
        $projectUrl = Yii::$app->request->get('project');

        $data = Project::find()->where(array("url" => $projectUrl))->one();

        if (empty($data)) throw new HttpException(404, 'Проект не найден');

        return $this->render('project', ['project' => $data]);
    }
}
