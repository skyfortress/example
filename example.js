window.requestAnimationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
})();


(function() {

    // Configs

    var BACKGROUND_COLOR      = 'rgba(8, 46, 85, 1)';
    var particleCount = 150;

    var canvas = document.getElementById("c");
    var ctx     = canvas.getContext('2d');

    var screenWidth = canvas.width  = window.innerWidth;
    var screenHeight = canvas.height = window.innerHeight;

    var startPoint = (Math.PI/180)*0;
    var endPoint = (Math.PI/180)*360;

    var cx,
        cy;

    var grad;
    var Collection = []; 

    

    var rand = function(min,max)
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    } 

    var Particle = function(){

        this.size = rand(5, 40);
        this.x = rand(this.size, screenWidth - this.size);
        this.y = rand(this.size, screenHeight - this.size);
        this.dx = rand(-100,100) / 100;
        this.dy = rand(-100,100) / 100;
        this.Alpha = rand(0,100) / 100;
        this.increasing = true;
    };
    Particle.prototype.adjustOpacity = function(){
        if(this.Alpha >= 0.98)
            this.increasing = false;
        if(this.Alpha <= 0.3)  
            this.increasing = true; 

        if(this.increasing)
            this.Alpha += 0.01; 
        else 
            this.Alpha -= 0.01; 
    };

    Particle.prototype.move = function(){
        if(this.x <= this.size || this.x >= screenWidth - this.size)
            this.dx *= -1;
        if(this.y <= this.size || this.y >= screenHeight - this.size)
            this.dy *= -1;

        this.x += this.dx;
        this.y += this.dy;
    };

    var Triangle = function(){
        Particle.apply(this, arguments);

        this.angle = 0;

        this.rotate = function(){
           this.angle += this.dx*Math.PI/180;  
        }

        this.draw = function(ctx){
            ctx.save();
            ctx.fillStyle = 'rgba(36,194,203, ' + this.Alpha + ')';
            ctx.beginPath();
            ctx.translate(this.x, this.y);
            ctx.rotate(this.angle);
            ctx.moveTo(0, -this.size);
            ctx.lineTo(this.size, this.size);
            ctx.lineTo(-this.size, this.size);
            ctx.fill();
            ctx.closePath(); 
            ctx.restore(); 
        }
    };
    Triangle.prototype = Object.create(Particle.prototype);
    Triangle.prototype.constructor = Triangle;

    var Square = function(){
        Particle.apply(this, arguments);

        this.angle = 0;

        this.rotate = function(){
           this.angle += this.dy*2*Math.PI/180;  
        }

        this.draw = function(ctx){
            ctx.save();
            ctx.fillStyle = 'rgba(200,10,80, ' + this.Alpha + ')';
            ctx.beginPath();
            ctx.translate(this.x, this.y);

            ctx.rotate(this.angle);
            ctx.moveTo(-this.size/2, -this.size/2);
            ctx.lineTo(this.size/2, -this.size/2);
            ctx.lineTo(this.size/2, this.size/2);
            ctx.lineTo(-this.size/2, this.size/2);

            ctx.fill();
            ctx.closePath(); 
            ctx.restore(); 
        }
    };
    Square.prototype = Object.create(Particle.prototype);
    Square.prototype.constructor = Square;

    var Circle = function(){
        Particle.apply(this, arguments);
        this.draw = function(ctx){
            ctx.save();
            ctx.fillStyle = 'rgba(100,150,220, ' + this.Alpha + ')';
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.size, startPoint, endPoint,true);
            ctx.fill();
            ctx.closePath();     
            ctx.restore(); 
        }
    };
    Circle.prototype = Object.create(Particle.prototype);
    Circle.prototype.constructor = Circle;

    var generateCollection = function(){
      var factory  = function(Object, count){
        for(var i = 0; i < count; i++)
          Collection.push(new Object);
      };  
      factory(Triangle, 30);
      factory(Circle, 30);
      factory(Square, 30);
      
    };

    var resize = function(e) {
        screenWidth  = canvas.width  = window.innerWidth;
        screenHeight = canvas.height = window.innerHeight;
        ctx     = canvas.getContext('2d');

        cx = canvas.width * 0.5,
        cy = canvas.height * 0.5;

        grad = ctx.createRadialGradient(cx, screenHeight, 0, cx, screenHeight, Math.sqrt(cx * cx + screenHeight * screenHeight));
        grad.addColorStop(0, 'rgba(0, 0, 0, 0)');
        grad.addColorStop(1, 'rgba(0, 0, 0, 0.7)');
        generateCollection();
    }



    window.addEventListener('resize', resize, false);
    resize(null);

    var drawBG = function(){
        ctx.save();
        ctx.fillStyle = BACKGROUND_COLOR;
        ctx.fillRect(0, 0, screenWidth, screenHeight);
        ctx.fillStyle = grad;
        ctx.fillRect(0, 0, screenWidth, screenHeight);
        ctx.restore();
    };


    var loop = function() {
        drawBG();
        for(var i = 0; i < Collection.length; i++){
            var point = Collection[i];
            point.adjustOpacity();
            point.move();
            if(point.rotate !== undefined)
               point.rotate(); 
            point.draw(ctx);
        } 
        //console.log("Frame generated");
        requestAnimationFrame(loop);
    };
    loop();
  

})();
