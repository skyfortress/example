var alignModal = function() {
    $('.modal').not('.product-gallery').each(function () {
        if ($(this).hasClass('in') === false) {
            $(this).show();
        }
        var contentHeight = $(window).height() - 60;
        var headerHeight = $(this).find('.modal-header').outerHeight() || 2;
        var footerHeight = $(this).find('.modal-footer').outerHeight() || 2;

        $(this).find('.modal-content').css({
            'max-height': function () {
                return contentHeight;
            }
        });

        $(this).find('.modal-body').css({
            'max-height': function () {
                return contentHeight - (headerHeight + footerHeight);
            }
        });

        $(this).find('.modal-dialog').addClass('modal-dialog-center').css({
            'margin-top': function () {
                return -($(this).outerHeight() / 2);
            },
            'margin-left': function () {
                return -($(this).outerWidth() / 2);
            }
        });
        if ($(this).hasClass('in') === false) {
            $(this).hide();
        }
    });
};

var updateCart = function(cart, callback){
    cart = $(cart);

    $.ajax({
        url: '/cart',
        type: 'GET',
        dataType: 'html',
        success: function (html) {
            cart.find('.cart-info').html(html);
            alignModal();
            if(typeof callback !== 'undefined')
                callback();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

var removeFromCart = function(el){
    row = $(el).closest('tr');
    row.remove();

    alignModal();
    updateCartTotal();

};

var addToCart = function(product_id, quantity) {
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    $.ajax({
        url: '/cart/add',
        type: 'POST',
        data: 'id=' + product_id + '&quantity=' + quantity,
        dataType: 'json',
        success: function (json) {
            $('.success, .warning, .attention, .information, .error').remove();

            if (json['success']) {
                $('html, body').animate({scrollTop: 0}, 'slow');
            }

            updateCart('#cartModal', function(){
                $('#cartModal').modal('show');
            });


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

var updateCartTotal = function(){
    var cart = $("#cartModal");
    var products = cart.find('.cart-product');
    var total = 0;

    products.each(function(){
        total += parseInt($(this).find('.price').attr('data-price')) * parseInt($(this).find('.quantity-count').text());
    });

    $('#cart-total').text(total);
};

var updateProductTotal = function(el, count){
    el = $(el).closest('.cart-product');
    var productTotal = parseInt(el.find('.price').attr('data-price')) * count;
    el.find('.total').text(productTotal + " грн.");
};

var synchronizeCart = function(){
    var cartProducts = $('#cartModal').find('.cart-product');

    var data = [];

    cartProducts.each(function(){
        var item = {};
        var id = parseInt($(this).attr('data-id'));

        item['id'] = id;
        item['quantity'] =  parseInt($(this).find('.quantity-count').text());

        data.push(item);
    });

    $.ajax({
        url: '/cart/update',
        type: 'POST',
        data: {products: JSON.stringify(data)},
        dataType: 'json',
        success: function (json) {
            $('#cart-total-widget').html(json['success']);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

$(function () {
    updateCart('#cartModal', function(){
        alignModal();
    });

    if ($(window).height() >= 320) {
        $(window).resize(alignModal).trigger("resize");
    }

    $('body').delegate('.count-increase', 'click', function () {
        var countSelector = $(this).parent().find('.quantity-count');
        var count = parseInt(countSelector.text());
        countSelector.text(++count);
        updateProductTotal(this, count);
        updateCartTotal();
    });

    $('body').delegate('.count-decrease', 'click', function () {
        var countSelector = $(this).parent().find('.quantity-count');
        var count = parseInt(countSelector.text());
        if (count !== 1){
            countSelector.text(--count);
            updateProductTotal(this, count);
        }
        updateCartTotal();
    });

    $($('#cartModal').on('hide.bs.modal', function() {
        synchronizeCart();
    }))
});

